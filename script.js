/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
DOM представляє структуру веб-сторінки як ієрархічне дерево об'єктів, доступне для маніпуляцій з використанням JavaScript, що дозволяє змінювати вміст,
стиль і поведінку сторінки в реальному часі. Він забезпечує зручний доступ до кожного елемента HTML,
атрибутів та тексту для створення динамічних інтерактивних веб-додатків.
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
За допомогою  innerHTML можна доданти HTML структуру, а innerText змінить тільки вміст тегу.
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
За допомогою методів getElementById, querySelector, getElementsByClassName, getElementsByTagName, найкращим є querySelector.
4. Яка різниця між nodeList та HTMLCollection?
NodeList містить будь-який тип
HTMLCollection містить лише елементи HTML
*/

/* Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).  */
const featureWayOne = document.querySelectorAll('.feature');
console.log(featureWayOne);
const featureWayTwo = document.getElementsByClassName('feature');
console.log(featureWayTwo);
featureWayOne.forEach(feature => {
    feature.style.textAlign = 'center';
})

/*  2. Змініть текст усіх елементів h2 на "Awesome feature".*/
const titel = document.querySelectorAll('h2');
titel.forEach(h2 => {
    h2.innerText ="Awesome feature";
});
/*  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!". */

const featureTitle =  document.querySelectorAll('.feature-title');
featureTitle.forEach(titel =>{
    titel.innerHTML = titel.innerHTML + "!";
});